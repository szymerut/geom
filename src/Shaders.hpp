#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

struct ShaderSource
{
    std::string vertexShader;
    std::string fragmentShader;
};

class Shader
{
public:
    Shader(const std::string& fileName)
    {
        std::ifstream file(fileName, std::ios::in);
        LoaderType loaderType{ LoaderType::None };

        if (file.fail())
        {
            std::cout << "Wrong file" << std::endl;
        }

        std::string line;
        std::stringstream text[2];
        while (std::getline(file, line))
        {
            if (line.find("#shader") != std::string::npos)
            {
                if (line.find("vertex") != std::string::npos)
                {
                    loaderType = LoaderType::Vertex;
                }
                else if (line.find("fragment") != std::string::npos)
                {
                    loaderType = LoaderType::Fragment;
                }
            }
            else
            {
                text[static_cast<int>(loaderType)] << line << '\n';
            }

        }

        file.close();

        vertShaderSource = text[0].str();
        fragShaderSource = text[1].str();

        compileShaders();
    };

    void use()
    {
        glUseProgram(ID);
    }
    
    void setUniformBool(const std::string& name, bool value) const
    {
        glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
    }

    void setUniformInt(const std::string& name, int value) const
    {
        glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
    }

    void setUniformFloat(const std::string& name, float value) const
    {
        glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
    }

    const unsigned int& getID() const
    {
        return ID;
    }

private:
    unsigned int ID;
    std::string vertShaderSource;
    std::string fragShaderSource;

    enum class LoaderType
    {
        None = -1,
        Vertex = 0,
        Fragment = 1
    };

    void createShader(int shaderType, GLuint& shader)
    {
        int success;

        shader = glCreateShader(shaderType);

        switch (shaderType)
        {
        case GL_VERTEX_SHADER:
        {
            const char* vertShaderArr = vertShaderSource.c_str();
            glShaderSource(shader, 1, &vertShaderArr, NULL);
            break;
        }
        case GL_FRAGMENT_SHADER:
        {
            const char* fragShaderArr = fragShaderSource.c_str();
            glShaderSource(shader, 1, &fragShaderArr, NULL);
            break;
        }
        default:
            break;
        }

        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

        if (!success)
        {
            int length;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
            char* message = static_cast<char*>(alloca(length * sizeof(char)));
            glGetShaderInfoLog(shader, 512, NULL, message);
            std::cout << "COMPILATION_FAILED\n" << message << std::endl;
        }
    }

    void compileShaders()
    {
        GLuint vertexShader, fragmentShader;
        createShader(GL_VERTEX_SHADER, vertexShader);
        createShader(GL_FRAGMENT_SHADER, fragmentShader);

        ID = glCreateProgram();
        glAttachShader(ID, vertexShader);
        glAttachShader(ID, fragmentShader);

        glLinkProgram(ID);

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
};
