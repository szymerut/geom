#target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

file(GLOB SOURCES "*.cpp" "*.hh")
file(GLOB STB "vendor/stb_image/*.cpp" "vendor/stb_image/*.hh")

add_executable(${PROJECT_NAME} ${SOURCES} ${STB})

target_sources(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/Shaders.hpp)

target_include_directories(${PROJECT_NAME}
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/vendor/stb_image
)

target_include_directories(${PROJECT_NAME}
    PUBLIC ${PROJECT_SOURCE_DIR}/vendor/glm/
    PUBLIC ${PROJECT_SOURCE_DIR}/vendor/glfw/include
    PUBLIC ${PROJECT_SOURCE_DIR}/vendor/glad/include
)

target_link_libraries(${PROJECT_NAME}
    glfw
    glad
)

source_group("Include files" FILES *.hpp)